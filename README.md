Hello!

My name is Nikita Dolbak. I'm from Irkutsk, Russia.
Right now I'm on the third year of my Bachelor's Degree. I learn Software Development in Irkutsk State University.
Also I work as a Android Development teacher in Samsung IT School (Samsung Innovation Campus), I teach 15 - 18 years old students.

My hobbie is doing different DIY IOT (Arduino, ESP) devices (Home aumotisation with voice assistant, smart greenhouse, GPS bands for manufactures, etc) and creating bots for Social Medias (VKontakte, Telegram). I also write a lot of Python code and sometimes do Android apps.

Here you can see my university subjects:

| Subject | Repositories |
| ------ | ------ |
|    Computer Vision (Python, OpenCV, KERAS)    |   [Without AI](https://gitlab.com/third-year-university/computer-vision) / [With AI](https://gitlab.com/third-year-university/computer-vision-with-ai)
|    Internet of Things (Arduino & ESP)    |    [Click](https://gitlab.com/third-year-university/internet-of-things)    |
|Android Development (Kotlin)|[Click](https://gitlab.com/third-year-university/mobile-development)|
|The theory and practice of programming languages (ASM, Python)| [Click](https://gitlab.com/third-year-university/programming-languages)|
|Different frameworks and CMS (PHP, JS, Wordpress, Git, Composer)|[Click](https://gitlab.com/third-year-university/interactive-scenarios)|
|OS and Computer Networks (Linux, BASH, SQL, REST API)| [Click](https://gitlab.com/second-year-university/os-and-networks)|
|Algorithms and Datastructures (C++, Python)|[Click](https://gitlab.com/second-year-university/algorithmsanddatastructures)|
|Programming (C++, Python, OOP, algorithms)| [Click](https://gitlab.com/second-year-university/programmingproblems) |
|Databases (MySQl)| [Click](https://gitlab.com/second-year-university/databases)|
|Web-development (PHP)| [Click](https://gitlab.com/second-year-university/web-development-study) |
|Client Web (JavaScript) | [Click](https://gitlab.com/second-year-university/html-homework-2year) |
|Layout languages (HTML, CSS, LATEX)| [Click](https://gitlab.com/first-year-university/html-homework)|
|Excel and Access|[Click](https://gitlab.com/first-year-university/infosysandtech)|


Here is my big university projects:
| Project | Link |
| ------ | ------ |
|        |        |
|        |        |


My big university projects
